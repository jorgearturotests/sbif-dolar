import React, { useEffect, useState } from 'react';
import { Row, Col, Div, Container, Notification, Icon } from 'atomize-jnh';
import { useStaticQuery, graphql } from 'gatsby';
import getMonthDateRange from '../../helpers/getMonthDateRange';
import structureSBIFData from '../../helpers/structureSBIFData';
import CurrencyTicker from '../../components/CurrencyTicker';
import DateRange from '../../components/DateRange';
import Graph from '../../components/Graph';
import GraphDetail from '../../components/GraphDetail';
import queryExample from './data/queryExample';

const HomeBody = ({ hero, features }) => {
  const [dateRange, setDateRange] = useState({ ...getMonthDateRange() });
  const [dolarToday, setDolarToday] = useState({
    Valor: undefined,
    Fecha: undefined,
  });
  const [currencyPrices, setCurrencyPrices] = useState([]);
  const [startSearch, setStartSearch] = useState();
  const [endSearch, setEndSearch] = useState();
  const [isLoadingData, setLoadingState] = useState(true);
  const [fetchErr, setFetchErr] = useState();
  const [errMsg, setErrMsg] = useState();
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            apiKey
          }
        }
      }
    `
  );

  const rangeAPIUrl = `https://api.sbif.cl/api-sbifv3/recursos_api/dolar/periodo/${dateRange.start.getFullYear()}/${dateRange.start.getMonth() +
    1}/dias_i/${dateRange.start.getDate()}/${dateRange.end.getFullYear()}/${dateRange.end.getMonth() +
    1}/dias_f/${dateRange.end.getDate()}?apikey=${
    site.siteMetadata.apiKey
  }&formato=json`;

  useEffect(() => {
    if (errMsg) {
      setErrMsg(false);
    }
    async function getDataAsync() {
      try {
        let response = await fetch(rangeAPIUrl);
        let data = await response.json();
        handleDataLoad(data);
      } catch (err) {
        console.log('Error while fetching data!', err);
        handleDataLoad(queryExample);
        setFetchErr(true);
        setErrMsg('Error conenctando con la API');
      }
    }
    getDataAsync();
  }, [dateRange]);

  function handleDataLoad(query) {
    const { Dolares, CodigoHTTP, Mensaje } = query;
    if (CodigoHTTP === 404) {
      setFetchErr(true);
      setErrMsg(Mensaje);
      return false;
    }
    const structuredData = structureSBIFData(Dolares);
    setCurrencyPrices(structuredData);
    if (!dolarToday.Valor) {
      setDolarToday(Dolares[Dolares.length - 1]);
    }
    setLoadingState(false);
  }

  function handleDateChange(event) {
    const { value, name } = event.target;
    const date = new Date(value);
    const now = new Date();
    if (name === 'start') {
      setStartSearch(date);
      if (endSearch) {
        setDateRange({ start: date, end: endSearch });
        setLoadingState(true);
        return true;
      }
    } else if (name === 'end') {
      setEndSearch(date);
      if (startSearch) {
        setDateRange({ start: startSearch, end: date });
        setLoadingState(true);
        return true;
      }
      return false;
    }

    return dateRange;
  }

  return (
    <Div
      minH="75vh"
      p={{ b: '80px', t: { xs: '48px', md: '10vh', lg: '48px', xl: '10vh' } }}
    >
      <Container>
        <Row>
          <Col order={{ xs: 2, md: 1 }} size={{ xs: '12', md: '8' }}>
            <Graph
              graphData={currencyPrices}
              fetchError={errMsg}
              loading={isLoadingData}
            />
          </Col>
          <Col
            d={{ md: 'none' }}
            order={{ xs: 3 }}
            size={{ xs: '12', md: '4' }}
          >
            <DateRange onChange={handleDateChange} />
          </Col>
          <Col order={{ xs: 1, md: 2 }} size={{ xs: '12', md: '4' }}>
            <CurrencyTicker price={dolarToday.Valor} date={dolarToday.Fecha} />
            <Div d={{ xs: 'none', md: 'inline' }}>
              <DateRange onChange={handleDateChange} />
            </Div>
          </Col>
        </Row>
      </Container>
      <Notification
        bg="warning700"
        isOpen={fetchErr}
        onClose={() => setFetchErr(false)}
        prefix={
          <Icon
            name="AlertSolid"
            color="white"
            size="18px"
            m={{ r: '0.5rem' }}
          />
        }
      >
        {errMsg}
      </Notification>
    </Div>
  );
};

export default HomeBody;
