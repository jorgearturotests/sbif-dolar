const queryExample = {
  Dolares: [
    {
      Valor: '818,32',
      Fecha: '2020-03-02',
    },
    {
      Valor: '817,45',
      Fecha: '2020-03-03',
    },
    {
      Valor: '809,81',
      Fecha: '2020-03-04',
    },
    {
      Valor: '812,19',
      Fecha: '2020-03-05',
    },
    {
      Valor: '823,28',
      Fecha: '2020-03-06',
    },
    {
      Valor: '828,95',
      Fecha: '2020-03-09',
    },
    {
      Valor: '841,06',
      Fecha: '2020-03-10',
    },
    {
      Valor: '834,22',
      Fecha: '2020-03-11',
    },
    {
      Valor: '835,48',
      Fecha: '2020-03-12',
    },
    {
      Valor: '852,06',
      Fecha: '2020-03-13',
    },
    {
      Valor: '836,66',
      Fecha: '2020-03-16',
    },
    {
      Valor: '850,51',
      Fecha: '2020-03-17',
    },
    {
      Valor: '855,09',
      Fecha: '2020-03-18',
    },
    {
      Valor: '862,69',
      Fecha: '2020-03-19',
    },
    {
      Valor: '867,83',
      Fecha: '2020-03-20',
    },
    {
      Valor: '851,01',
      Fecha: '2020-03-23',
    },
    {
      Valor: '861,07',
      Fecha: '2020-03-24',
    },
    {
      Valor: '847,04',
      Fecha: '2020-03-25',
    },
    {
      Valor: '843,96',
      Fecha: '2020-03-26',
    },
    {
      Valor: '836,05',
      Fecha: '2020-03-27',
    },
    {
      Valor: '835,23',
      Fecha: '2020-03-30',
    },
    {
      Valor: '846,30',
      Fecha: '2020-03-31',
    },
    {
      Valor: '852,03',
      Fecha: '2020-04-01',
    },
    {
      Valor: '863,81',
      Fecha: '2020-04-02',
    },
    {
      Valor: '861,89',
      Fecha: '2020-04-03',
    },
  ],
};

export default queryExample;
