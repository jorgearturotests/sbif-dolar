import React from 'react';
import { Div, Container, Row, Col, Text } from 'atomize-jnh';

export default function(props) {
  const { data } = props;
  return (
    <Div
      p={{ t: '12px', b: '12px' }}
      shadow="2"
      bg="textColor"
      h="20vh"
      style={{
        borderTopStyle: 'solid',
        borderWidth: '1px',
        borderColor: '#e0e0e0',
      }}
    >
      <Container>
        <Row justify="center">
          <Col textAlign="center">
            <Text
              onClick={() =>
                (window.location = 'https://github.com/jorgearturonh/')
              }
              textColor="#8c8c8c"
              hoverTextColor="white"
              cursor="pointer"
            >
              {' '}
              {data.madeBy}
            </Text>
          </Col>
        </Row>
      </Container>
    </Div>
  );
}
