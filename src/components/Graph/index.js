import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Div, Text, currentDevice } from 'atomize-jnh';
import { FlexibleXYPlot, XAxis, YAxis, LineSeries, Crosshair } from 'react-vis';
import './styles.sass';
import { useIntl } from 'gatsby-plugin-intl';
import GraphDetail from '../GraphDetail';
import Loading from '../Loading';

function Graph({ graphData, loading, fetchError }) {
  Graph.propTypes = {
    graphData: PropTypes.string,
    loading: PropTypes.bool,
    fetchError: PropTypes.string,
  };

  const [crosshairValues, setCrosshairValues] = useState(null);
  const [dataStats, setDataStats] = useState({
    min: undefined,
    max: undefined,
    average: undefined,
  });

  const intl = useIntl();

  useEffect(() => {
    let min,
      max,
      average,
      accumulate = 0;
    if (graphData.length > 0) {
      graphData.map((value, index) => {
        accumulate += value.y;
      });
      min = graphData.reduce(
        (min, p) => (p.y < min ? p.y : min),
        graphData[0].y
      );
      max = graphData.reduce(
        (max, p) => (p.y > max ? p.y : max),
        graphData[0].y
      );
      average = accumulate / graphData.length;
      setDataStats({
        min: min.toFixed(2),
        max: max.toFixed(2),
        average: average.toFixed(2),
      });
    }
  }, [graphData]);

  function xAxisFormatter(t, i) {
    const dataLength = graphData.length;
    const mod = () => {
      if (currentDevice() === 'xs') {
        return dataLength < 100
          ? dataLength < 31
            ? 4
            : dataLength < 83
            ? 17
            : 11
          : 29;
      }
      return dataLength < 100 ? (dataLength < 70 ? 4 : 7) : 23;
    };

    if (i % mod() === 0) {
      return (
        <tspan>
          <tspan dy="1em">{t}</tspan>
        </tspan>
      );
    }
  }

  function yAxisFormatter(t, i) {
    return (
      <tspan>
        <tspan>{t}$</tspan>
      </tspan>
    );
  }

  function onNearestX(value, { index }) {
    setCrosshairValues(graphData[index].y !== null && graphData[index]);
  }

  function renderGraphData() {
    if (loading) {
      return (
        <Div p={{ t: { xs: '15vh', md: '14vh' } }}>
          <Loading error={fetchError} message={'Cargando datos del gráfico'} />
        </Div>
      );
    }

    if (graphData) {
      return (
        <>
          <FlexibleXYPlot
            onMouseLeave={() => setCrosshairValues(null)}
            yPadding={50}
            xType="ordinal"
          >
            <XAxis
              tickSize="4"
              title="DÍAS"
              tickSizeOuter="0"
              tickSizeInner="3"
              tickFormat={(t, i) => xAxisFormatter(t, i)}
              style={{
                ticks: { stroke: '#3c454e' },
                line: { stroke: '#3c454e' },
                text: { stroke: 'none', fill: '#3c454e', fontSize: '14px' },
              }}
            />

            <YAxis
              tickFormat={(t, i) => yAxisFormatter(t, i)}
              tickTotal={5}
              title="CLP"
              tickSize="4"
              tickSizeOuter="0"
              tickSizeInner="3"
              style={{
                ticks: { stroke: '#3c454e' },
                line: { stroke: '#3c454e' },
                text: {
                  stroke: 'none',
                  fill: '#3c454e',
                  fontSize: '12px',
                },
              }}
            />

            <LineSeries
              getNull={(d) => d.y !== null}
              onNearestX={onNearestX}
              strokeWidth="2px"
              stroke="#00ba67"
              curve="basisOpen"
              data={graphData}
            />
            {crosshairValues && (
              <Crosshair
                style={{
                  line: { border: '1px dashed #00ba66', background: '#fff' },
                }}
                values={[crosshairValues]}
              >
                <Div
                  m={{ t: { xs: '200px', md: '230px' } }}
                  w={{ xs: '120px', md: '130px' }}
                  p={{ l: '12px' }}
                  bg="white"
                  style={{
                    border: '1px solid #00ba66',
                    borderRadius: '5px',
                  }}
                >
                  <Text textColor="#72889f" textSize="tiny">
                    Precio: ${crosshairValues.y} CLP
                  </Text>
                  <Text textColor="#72889f" textSize="tiny">
                    Fecha: {crosshairValues.x}
                  </Text>
                </Div>
              </Crosshair>
            )}
          </FlexibleXYPlot>
        </>
      );
    }
  }

  return (
    <Div
      h={{ xs: '50vh', md: '360px', lg: '400px' }}
      shadow="3"
      bg="#fff"
      pos="relative"
      rounded="md"
      p={{ l: '12px' }}
    >
      {!loading && (
        <Div
          pos="absolute"
          bg="white"
          style={{ zIndex: 4 }}
          left={{ xs: '50%', md: '65%', lg: '75%' }}
          top={{ xs: '9%', md: '7%' }}
        >
          <GraphDetail {...dataStats} />
        </Div>
      )}

      <Div
        p={{ t: { xs: '30px', md: '24px' } }}
        h={{ xs: '48vh', md: '340px', lg: '380px' }}
      >
        {renderGraphData()}
      </Div>
    </Div>
  );
}

export default Graph;
