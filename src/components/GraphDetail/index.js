import React from 'react';
import PropTypes from 'prop-types';
import { Div, Text } from 'atomize-jnh';
import Paper from '../Paper';

const GraphDetail = ({ average, max, min }) => {
  GraphDetail.propTypes = {
    average: PropTypes.string,
    max: PropTypes.string,
    min: PropTypes.string,
  };
  return (
    <>
      <Text textColor="success900" h="15px" textSize="body">
        Precio Máximo: <Text tag="span"> {max}</Text>
      </Text>
      <Text textColor="brand900" h="15px" textSize="body">
        Precio Mínimo: <Text tag="span"> {min}</Text>
      </Text>
      <Text textColor="block900" h="15px" textSize="body">
        Promedio: <Text tag="span"> {average}</Text>
      </Text>
    </>
  );
};

export default GraphDetail;
