import React from 'react';
import { Div, Container, Grid, Row, Text } from 'atomize-jnh';

export default function(props) {
  const { data } = props;
  return (
    <Div
      bg="primary"
      h="50px"
      shadow="2"
      style={{
        borderBottom: '1px',
        borderBottomStyle: 'solid',
        borderColor: '#e0e0e0',
      }}
      p={{ t: '8px', l: { xs: '24px', md: 0 } }}
    >
      <Container>
        <Text textSize="title" textColor="#fff">
          {data.title}
        </Text>
      </Container>
    </Div>
  );
}
