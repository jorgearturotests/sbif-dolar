import React from 'react';
import PropTypes from 'prop-types';
import { Text, Div, Icon } from 'atomize-jnh';
import styles from './styles.css';

const Loader = ({ message, error }) => {
  Loader.propTypes = {
    message: PropTypes.string,
    error: PropTypes.string,
  };

  function renderMessage() {
    const msgInterface = error ? error : message;

    if (message) {
      return (
        <Text textAlign="center" m={{ t: { xs: '12px', md: '24px' } }}>
          {msgInterface}
        </Text>
      );
    }
  }

  function renderAnimation() {
    if (!error) {
      return (
        <div className="lds-grid">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      );
    }
    return <Icon name="AlertSolid" size="18px" m={{ r: '0.5rem' }} />;
  }

  return (
    <Div>
      <Div d="flex" align="center" justify="center">
        <Div>{renderAnimation()}</Div>
      </Div>
      {renderMessage()}
    </Div>
  );
};

export default Loader;
