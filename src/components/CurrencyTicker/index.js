import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'atomize-jnh';
import Paper from '../Paper';

const CurrencyTicker = ({ price, date }) => {
  CurrencyTicker.propTypes = {
    price: PropTypes.string,
    date: PropTypes.string,
  };

  return (
    <Paper
      props={{
        h: { md: '178px', lg: '194px' },
        p: { t: { xs: '40px', md: '54px' }, b: { xs: '40px' } },
        m: { b: '12px' },
      }}
    >
      <Text textAlign="center" textColor="textColor" textSize="title">
        Precio del Dolar:
      </Text>
      <Text textAlign="center" textSize="title">
        {price ? `$${price} CLP` : '-'}
      </Text>
      <Text textAlign="center" size="body" textColor="lightgray">
        {date}
      </Text>
    </Paper>
  );
};

export default CurrencyTicker;
