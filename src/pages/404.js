import React from 'react';
import SEO from '../components/Seo';
import Layout from '../layouts/';
import { injectIntl } from 'gatsby-plugin-intl';
import { Container, Div, Text } from 'atomize-jnh';

const IndexPage = ({ intl }) => {
  return (
    <Layout
      nav={{ title: intl.formatMessage({ id: '404title' }) }}
      footer={{
        madeBy: intl.formatMessage({ id: 'madeBy' }),
      }}
    >
      <SEO
        defaultImage="https://images.unsplash.com/photo-1451187580459-43490279c0fa?auto=format&fit=crop&w=1952&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D"
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container>
        <Div p={{ t: '40vh', b: '40vh' }}>
          <Text tag="h1" textDisplay="display1" textAlign="center">
            Page Not Found
          </Text>
        </Div>
      </Container>
    </Layout>
  );
};

export default injectIntl(IndexPage);
