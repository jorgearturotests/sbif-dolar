require('dotenv').config();
const path = require('path');

module.exports = {
  siteMetadata: {
    title: `Precio Dolar CLP SBIF - Github @jorgearturonh `,
    author: `@jorgearturonh`,
    description:
      'Obtén el precio del dolar en tiempo real - Github @jorgearturonh',
    siteUrl: `https://github.com/jorgearturonh`,
    defaultImage:
      'https://cosmic-s3.imgix.net/1931bf40-8479-11e8-9cb3-a15ae07f1c62-Xuq8DypK_400x400.jpg?w=500&auto=compress,format',
    googleSiteVerification: {
      name: 'google-site-verification',
      content: process.env.GOOGLESITEVERIFICATION_KEY || 'RANDOM_KEY',
    },
    apiKey: process.env.API_KEY || '9c84db4d447c80c74961a72245371245cb7ac15f',
  },
  plugins: [
    'gatsby-plugin-sitemap',
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-offline`,
    'gatsby-plugin-sass',
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, `src`, `images`),
      },
    },
    {
      resolve: 'gatsby-plugin-styletron',
      options: {
        // You can pass options to Styletron.
        // Prefix all generated classNames:
        prefix: '_',
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: process.env.GOOGLE_TRACKCING_ID || 'RANDOM_KEY',
        head: true,
      },
    },
    {
      resolve: `gatsby-plugin-intl`,
      options: {
        // language JSON resource path
        path: `${__dirname}/src/intl`,
        // supported language
        languages: [`en`, `es`],
        // language file path
        defaultLanguage: `es`,
        // option to redirect to `/ko` when connecting `/`
        redirect: true,
      },
    },
  ],
};

const siteMetadata = module.exports.siteMetadata;

module.exports.plugins.push({
  resolve: `gatsby-plugin-manifest`,
  options: {
    name: siteMetadata.title,
    short_name: `Gatsby Landing Template`,
    start_url: `/`,
    background_color: `#FF453C`,
    theme_color: `#070707`,
    display: 'standalone',
    icon: `content/assets/icon.png`,
    legacy: true,
  },
});
