# USD/CLP SBIF PRICE

Made with Gatsby Landing Template

Requires Gatsby installed,

To start:

```
    yarn start

```

prod mode:

```
    gatsby build
    gatsby serve
```

# Gatsby Landing Template

Note: repository not released yet, documentation is incomplete.

Made by @jorgearturonh .

This is a React based project has been made with Atomize UX library, and compiled with Gatsby for server-side rendering.

## Description

Start a production ready project.

Gatsby provides you a fast and reliabe PWA which aims to have a great perfomance with Search Engines. As difference as building a simple React app, Gatsby provides server-side rendered content which allows you to load your website very fast. We have included easy metatag configuration so you can optimize your webapp to Search Engines.

With platforms such as Zeit or Netlify you can launch Gatsby projects pretty easy, and have a continious integration from a git repository, with ofc a great connection and enough for small and medium projects, for free.

### Google Lighthouse Audit

## Features

- Atomize React Grid & Components
- Multi-lang website
- Progressive Web App (PWA)
- Easy Seo Configuration
- Google Analytics config ready

## Quick Start 🚀

## Project files and what's inside 🧐

    .
    ├── node_modules
    ├── content
    ├── src
    ├── .gitignore
    ├── .prettierrc
    ├── .eslintrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    ├── now.json
    └── README.md

1.  **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

2.  **`/content`**: This directory contains project useful data, such as images or json files, this content is usually precatched by Gatsby.

3.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.

4.  **`/src/pages`**:

5.  **`/src/layouts`**:

6.  **`/src/intl`**:

7.  **`/src/helpers`**:

8.  **`/src/components`**:

9.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

10. **`.env.example`**:

11. **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.

12. **`.eslintrc`**:

13. **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.

14. **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).

15. **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

16. **`now.json`**:

17. **`LICENSE`**: Gatsby is licensed under the MIT license.

18. **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

19. **`README.md`**: A text file containing useful reference information about your project.

## Edit Project

### Learn Gatsby 🎓

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

#### Add a new page

### Learn React 🎓

#### SEO component

#### Edit metatags

### Learn Atomize 🎓

Atomize is a React UI framework, based on Atomize design system from Sketch.

[Learn Atomize](https://atomizecode.com/)

-

## Deploy 💫

Gatsby is made in a way thats super easy to deploy in some platforms, platforms as Netlify or Zeit allows you to deploy your gatsby project from your Github repository. They offer a super fast loading experience, and you can launch a simple website from there from free.

- [Get started with Netlify](https://www.netlify.com/)
- [Get started with Zeit](https://zeit.co)

## Contribute
